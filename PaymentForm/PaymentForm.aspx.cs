﻿using PaymentForm.Controller;
using PaymentForm.Model;
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PaymentForm
{
    public partial class PaymentForm : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                AddMonths();
                AddYears();
                AddCurrency();
                AddCountryCodes();
            }
        }

        private void AddYears()
        {
            for (int i = DateTime.Now.Year; i < DateTime.Now.Year + 20; i++)
                ddlYearExpire.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        private void AddMonths()
        {
            for (int i = 1; i <= 12; i++)
                ddlMonthExpire.Items.Add(new ListItem(i.ToString(), i.ToString()));
            ddlMonthExpire.SelectedValue = DateTime.Now.Month.ToString();
        }

        private void AddCurrency()
        {
            //TODO: Call a service to get all Currency
            ddlCurrency.Items.Add(new ListItem("Euro (EUR)", "978"));
            ddlCurrency.Items.Add(new ListItem("Dolar (USD)", "840"));
            ddlCurrency.Items.Add(new ListItem("UK Pound (GBP)", "826"));
            ddlCurrency.SelectedValue = "978";
        }

        private void AddCountryCodes()
        {
            //TODO: Call a service to get all Country Code
            ddlCountryCode.Items.Add(new ListItem("Ireland", "372"));
            ddlCountryCode.Items.Add(new ListItem("Brazil", "076"));
            ddlCountryCode.Items.Add(new ListItem("United Kingdom", "826"));
            ddlCountryCode.SelectedValue = ddlCountryCode.Items[0].Value;
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            Payment payment = new Payment(
                tbAmount.Text,
                ddlCurrency.SelectedValue,
                DateTime.Now,
                HttpContext.Current.Request.Url.AbsoluteUri,
                rbEchoAVSCheckResultTrue.Checked,
                rbEchoCV2CheckResultTrue.Checked,
                rbEchoThreeDSecureAuthenticationCheckResultTrue.Checked,
                rbEchoFraudProtectionCheckResultTrue.Checked,
                rbEchoCardTypeTrue.Checked,
                rbEchoCardNumberFirstSixTrue.Checked,
                rbEchoCardNumberLastFourTrue.Checked,
                rbEchoAVSCheckResultTrue.Checked,
                rbEchoAVSCheckResultTrue.Checked,
                tbOrderDescription.Text,
                tbCustomerName.Text,
                tbAddress1.Text,
                tbAddress2.Text,
                tbAddress3.Text,
                tbAddress4.Text,
                tbCity.Text,
                tbState.Text,
                tbPostCode.Text,
                ddlCountryCode.SelectedValue,
                tbEmailAddress.Text,
                tbPhoneNumber.Text,
                rbEmailAddressEditableTrue.Checked,
                rbPhoneNumberEditableTrue.Checked,
                rbCV2MandatoryTrue.Checked,
                rbAddress1MandatoryTrue.Checked,
                rbCityMandatoryTrue.Checked,
                rbPostCodeMandatoryTrue.Checked,
                rbStateMandatoryTrue.Checked,
                rbCountryMandatoryTrue.Checked
            );
            payment.HashDigest = HashController.GetHashDigest(payment);

            HttpResponse response = HttpContext.Current.Response;
            response.Clear();
            response.Write(payment.GetHtmlFormData());
            response.End();
        }

    }
}

