﻿using System.Collections.Specialized;

namespace PaymentForm.Interface
{
    public interface IHashable
    {
        NameValueCollection ToCollection();
    }
}
