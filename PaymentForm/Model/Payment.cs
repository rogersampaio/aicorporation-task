﻿using PaymentForm.Interface;
using System;
using System.Collections.Specialized;
using System.Text;

namespace PaymentForm.Model
{
    public class Payment : IHashable
    {
        public string HashDigest { get; internal set; }
        public string Amount { get; internal set; }
        public string Currency { get; internal set; }
        public string TransactionDateTime { get; internal set; }
        public string AbsoluteUri { get; internal set; }
        public bool EchoAVSCheckResult { get; internal set; }
        public bool EchoCV2CheckResult { get; internal set; }
        public bool EchoThreeDSecureAuthenticationCheckResult { get; internal set; }
        public bool EchoFraudProtectionCheckResult { get; internal set; }
        public bool EchoCardType { get; internal set; }
        public bool EchoCardNumberFirstSix { get; internal set; }
        public bool EchoCardNumberLastFour { get; internal set; }
        public bool EchoCardExpiryDate { get; internal set; }
        public bool EchoDonationAmount { get; internal set; }
        public string OrderDescription { get; internal set; }
        public string CustomerName { get; internal set; }
        public string Address1 { get; internal set; }
        public string Address2 { get; internal set; }
        public string Address3 { get; internal set; }
        public string Address4 { get; internal set; }
        public string City { get; internal set; }
        public string State { get; internal set; }
        public string PostCode { get; internal set; }
        public string CountryCode { get; internal set; }
        public string EmailAddress { get; internal set; }
        public string PhoneNumber { get; internal set; }
        public bool EmailAddressEditable { get; internal set; }
        public bool PhoneNumberEditable { get; internal set; }
        public bool CV2Mandatory { get; internal set; }
        public bool Address1Mandatory { get; internal set; }
        public bool CityMandatory { get; internal set; }
        public bool PostCodeMandatory { get; internal set; }
        public bool StateMandatory { get; internal set; }
        public bool CountryMandatory { get; internal set; }

        public Payment(string amount,
            string currency,
            DateTime transactionDateTime,
            string absoluteUri,
            bool echoAVSCheckResult,
            bool echoCV2CheckResult,
            bool echoThreeDSecureAuthenticationCheckResult,
            bool echoFraudProtectionCheckResult,
            bool echoCardType,
            bool echoCardNumberFirstSix,
            bool echoCardNumberLastFour,
            bool echoCardExpiryDate,
            bool echoDonationAmount,
            string orderDescription,
            string customerName,
            string address1,
            string address2,
            string address3,
            string address4,
            string city,
            string state,
            string postCode,
            string countryCode,
            string emailAddress,
            string phoneNumber,
            bool emailAddressEditable,
            bool phoneNumberEditable,
            bool cv2Mandatory,
            bool address1Mandatory,
            bool cityMandatory,
            bool postCodeMandatory,
            bool stateMandatory,
            bool countryMandatory
            )
        {
            Amount = amount;
            Currency = currency;
            TransactionDateTime = transactionDateTime.ToString("yyyy-MM-dd HH:mm:ss zzz");
            AbsoluteUri = absoluteUri;
            EchoAVSCheckResult = echoAVSCheckResult;
            EchoCV2CheckResult = echoCV2CheckResult;
            EchoThreeDSecureAuthenticationCheckResult = echoThreeDSecureAuthenticationCheckResult;
            EchoFraudProtectionCheckResult = echoFraudProtectionCheckResult;
            EchoCardType = echoCardType;
            EchoCardNumberFirstSix = echoCardNumberFirstSix;
            EchoCardNumberLastFour = echoCardNumberLastFour;
            EchoCardExpiryDate = echoCardExpiryDate;
            EchoDonationAmount = echoDonationAmount;
            OrderDescription = orderDescription;
            CustomerName = customerName;
            Address1 = address1;
            Address2 = address2;
            Address3 = address3;
            Address4 = address4;
            City = city;
            State = state;
            PostCode = postCode;
            CountryCode = countryCode;
            EmailAddress = emailAddress;
            PhoneNumber = phoneNumber;
            EmailAddressEditable = emailAddressEditable;
            PhoneNumberEditable = phoneNumberEditable;
            CV2Mandatory = cv2Mandatory;
            Address1Mandatory = address1Mandatory;
            CityMandatory = cityMandatory;
            PostCodeMandatory = postCodeMandatory;
            StateMandatory = stateMandatory;
            CountryMandatory = countryMandatory;
        }

        internal string GetHtmlFormData()
        {
            NameValueCollection data = ToCollection();
            data.Add("HashDigest", HashDigest);

            StringBuilder s = new StringBuilder();
            s.Append("<html>");
            s.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
            s.AppendFormat("<form name='form' action='{0}' method='post'>", "https://mms.payvector.net/Pages/PublicPages/PaymentForm.aspx");
            foreach (string key in data)
            {
                if (key != "PreSharedKey" && key != "Password")
                    s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", key, data[key]);
            }
            s.Append("</form></body></html>");
            return s.ToString();
        }

        /// <summary>
        /// The collection to create the Hash, it must be in the specified order
        /// </summary>
        /// <returns></returns>
        public NameValueCollection ToCollection()
        {
            return new NameValueCollection
            {
                { "PreSharedKey", "5XNm04UE0/EaXDSeKa4Fw29hTzieAl32uO4=" },
                { "MerchantID", "Progra-7702818" },
                { "Password", "G6CH6Z90I2" },
                { "Amount", Amount },
                { "CurrencyCode", Currency },
                { "EchoAVSCheckResult", (EchoAVSCheckResult ? "true" : "false") },
                { "EchoCV2CheckResult", (EchoCV2CheckResult ? "true" : "false") },
                { "EchoThreeDSecureAuthenticationCheckResult", (EchoThreeDSecureAuthenticationCheckResult ? "true" : "false") },
                { "EchoFraudProtectionCheckResult", (EchoFraudProtectionCheckResult ? "true" : "false") },
                { "EchoCardType", (EchoCardType ? "true" : "false") },
                { "EchoCardNumberFirstSix", (EchoCardNumberFirstSix ? "true" : "false") },
                { "EchoCardNumberLastFour", (EchoCardNumberLastFour ? "true" : "false") },
                { "EchoCardExpiryDate", (EchoCardExpiryDate ? "true" : "false") },
                { "EchoDonationAmount", (EchoDonationAmount ? "true" : "false") },
                //{ "AVSOverridePolicy", "" },
                //{ "CV2OverridePolicy", "" },
                //{ "ThreeDSecureOverridePolicy", "" },
                { "OrderID", "Order-1234" },
                { "TransactionType", "SALE" },
                { "TransactionDateTime", TransactionDateTime},
                //{ "DisplayCancelButton", "" },
                { "CallbackURL", AbsoluteUri },
                { "OrderDescription", OrderDescription },
                //{ "LineItemSalesTaxAmount", "" },
                //{ "LineItemSalesTaxDescription", "" },
                //{ "LineItemQuantity", "" },
                //{ "LineItemAmount", "" },
                //{ "LineItemDescription", "" },
                { "CustomerName", CustomerName },
                //{ "DisplayBillingAddress", "" },
                { "Address1", Address1 },
                { "Address2", Address2 },
                { "Address3", Address3 },
                { "Address4", Address4 },
                { "City", City },
                { "State", State },
                { "PostCode", PostCode },
                { "CountryCode", CountryCode },
                { "EmailAddress", EmailAddress },
                { "PhoneNumber", PhoneNumber },
                //{ "DateOfBirth", "" },
                //{ "DisplayShippingDetails", "" },
                { "ShippingName", "" },
                { "ShippingAddress1", "" },
                { "ShippingAddress2", "" },
                { "ShippingAddress3", "" },
                { "ShippingAddress4", "" },
                { "ShippingCity", "" },
                { "ShippingState", "" },
                { "ShippingPostCode", "" },
                { "ShippingCountryCode", "" },
                { "ShippingEmailAddress", "" },
                { "ShippingPhoneNumber", "" },
                //{ "CustomerNameEditable", "" },
                { "EmailAddressEditable", (EmailAddressEditable ? "true" : "false") },
                { "PhoneNumberEditable", (PhoneNumberEditable ? "true" : "false") },
                //{ "DateOfBirthEditable", "" },
                { "CV2Mandatory", (CV2Mandatory ? "true" : "false") },
                { "Address1Mandatory", (Address1Mandatory ? "true" : "false") },
                { "CityMandatory", (CityMandatory ? "true" : "false") },
                { "PostCodeMandatory", (PostCodeMandatory ? "true" : "false") },
                { "StateMandatory", (StateMandatory ? "true" : "false") },
                { "CountryMandatory", (CountryMandatory ? "true" : "false") },
                //{ "ShippingAddress1Mandatory", "" },
                //{ "ShippingCityMandatory", "" },
                //{ "ShippingPostCodeMandatory", "" },
                //{ "ShippingStateMandatory", "" },
                //{ "ShippingCountryMandatory", "" },
                { "ResultDeliveryMethod", "POST" },
                { "ServerResultURL", "" },
                { "PaymentFormDisplaysResult", "" },
                { "ServerResultURLCookieVariables", "" },
                { "ServerResultURLFormVariables", "" },
                { "ServerResultURLQueryStringVariables", "" },
                { "PrimaryAccountName", "" },
                { "PrimaryAccountNumber", "" },
                //{ "PrimaryAccountDateOfBirth", "" },
                { "PrimaryAccountPostCode", "" },
                { "Skin", "" }
                //{ "PaymentFormContentMode", "" },
                //{ "BreakoutOfIFrameOnCallback", "" }
            };
        }

    }
}