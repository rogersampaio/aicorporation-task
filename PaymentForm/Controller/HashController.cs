﻿using PaymentForm.Interface;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace PaymentForm.Controller
{
    public static class HashController
    {
        public static string GetHashDigest<T>(T hashable) where T : IHashable
        {
            StringBuilder result = new StringBuilder();
            var data = hashable.ToCollection();
            int qtd = 0;
            foreach (string key in data)
            {
                qtd++;
                if (qtd == data.Count) //Last property
                    result.AppendFormat("{0}={1}", key, data[key]);
                else
                    result.AppendFormat("{0}={1}&", key, data[key]);
            }
            return GetHashDigest(result.ToString());
        }

        public static string GetHashDigest(string hashInput)
        {
            var hash = (new SHA1Managed()).ComputeHash(Encoding.UTF8.GetBytes(hashInput));
            return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
        }
    }
}