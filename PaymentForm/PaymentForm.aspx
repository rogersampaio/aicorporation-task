﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentForm.aspx.cs" Inherits="PaymentForm.PaymentForm" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>AI Corporation Payment Form</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="Styles/PaymentForm.css" rel="stylesheet" />
</head>
<body>
    <form runat="server">
        <div class="container card mainDiv">
            <h2>AI Corporation</h2>
            <h4>Payment Form</h4>
            <br />
            <div class="form-group">
                Expire Date*:&nbsp;
                <asp:DropDownList ID="ddlMonthExpire" runat="server" class="btn dropdown-toggle" required="true"></asp:DropDownList>&nbsp;
                <asp:DropDownList ID="ddlYearExpire" runat="server" class="btn dropdown-toggle" required="true"></asp:DropDownList>
            </div>
            <div class="form-group">
                Amount*:
                <asp:TextBox ID="tbAmount" class="form-control tbAmount" type="number" min="1" max="99999999999999" runat="server" required="true"></asp:TextBox>
            </div>
            <div class="form-group">
                Currency*:&nbsp;
                <asp:DropDownList ID="ddlCurrency" runat="server" class="btn dropdown-toggle" required="true"></asp:DropDownList>
            </div>
            <div class="form-group">
                Echo AVS Check Result:&nbsp;
                <asp:RadioButton ID="rbEchoAVSCheckResultTrue" runat="server" GroupName="EchoAVSCheckResult" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbEchoAVSCheckResultFalse" runat="server" GroupName="EchoAVSCheckResult" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                Echo CV2 Check Result:&nbsp;
                <asp:RadioButton ID="rbEchoCV2CheckResultTrue" runat="server" GroupName="EchoCV2CheckResult" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbEchoCV2CheckResultFalse" runat="server" GroupName="EchoCV2CheckResult" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                Echo ThreeDSecure Authentication Check Result:&nbsp;
                <asp:RadioButton ID="rbEchoThreeDSecureAuthenticationCheckResultTrue" runat="server" GroupName="EchoThreeDSecureAuthenticationCheckResult" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbEchoThreeDSecureAuthenticationCheckResultFalse" runat="server" GroupName="EchoThreeDSecureAuthenticationCheckResult" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                EchoFraudProtectionCheckResult:&nbsp;
                <asp:RadioButton ID="rbEchoFraudProtectionCheckResultTrue" runat="server" GroupName="EchoFraudProtectionCheckResult" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbbEchoFraudProtectionCheckResultFalse" runat="server" GroupName="EchoFraudProtectionCheckResult" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                Echo Card Type:&nbsp;
                <asp:RadioButton ID="rbEchoCardTypeTrue" runat="server" GroupName="EchoCardType" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbEchoCardTypefalse" runat="server" GroupName="EchoCardType" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                Echo Card Number First Six:&nbsp;
                <asp:RadioButton ID="rbEchoCardNumberFirstSixTrue" runat="server" GroupName="EchoCardNumberFirstSix" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbEchoCardNumberFirstSixFalse" runat="server" GroupName="EchoCardNumberFirstSix" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                Echo Card Number Last Four:&nbsp;
                <asp:RadioButton ID="rbEchoCardNumberLastFourTrue" runat="server" GroupName="EchoCardNumberLastFour" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbEchoCardNumberLastFourFalse" runat="server" GroupName="EchoCardNumberLastFour" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                Echo Card Expiry Date:&nbsp;
                <asp:RadioButton ID="rbEchoCardExpiryDateTrue" runat="server" GroupName="EchoCardExpiryDate" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbEchoCardExpiryDateFalse" runat="server" GroupName="EchoCardExpiryDate" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                Echo DonationAmount:&nbsp;
                <asp:RadioButton ID="rbEchoDonationAmountTrue" runat="server" GroupName="EchoDonationAmount" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbEchoDonationAmountFalse" runat="server" GroupName="EchoDonationAmount" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                Order Description:
                <asp:TextBox ID="tbOrderDescription" class="form-control" type="text" MaxLength="256" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                Customer Name:
                <asp:TextBox ID="tbCustomerName" class="form-control" type="text" MaxLength="100" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                Address 1:
                <asp:TextBox ID="tbAddress1" class="form-control" type="text" MaxLength="100" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                Address 2:
                <asp:TextBox ID="tbAddress2" class="form-control" type="text" MaxLength="50" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                Address 3:
                <asp:TextBox ID="tbAddress3" class="form-control" type="text" MaxLength="50" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                Address 4:
                <asp:TextBox ID="tbAddress4" class="form-control" type="text" MaxLength="50" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                City:
                <asp:TextBox ID="tbCity" class="form-control" type="text" MaxLength="50" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                State:
                <asp:TextBox ID="tbState" class="form-control" type="text" MaxLength="50" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                PostCode:
                <asp:TextBox ID="tbPostCode" class="form-control" type="text" MaxLength="50" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                Country Code:
                <asp:DropDownList ID="ddlCountryCode" runat="server" class="btn dropdown-toggle"></asp:DropDownList>
            </div>
            <div class="form-group">
                EmailAddress:
                <asp:TextBox ID="tbEmailAddress" class="form-control" type="text" MaxLength="100" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                PhoneNumber:
                <asp:TextBox ID="tbPhoneNumber" class="form-control" type="text" MaxLength="30" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                Email Address Editable:&nbsp;
                <asp:RadioButton ID="rbEmailAddressEditableTrue" runat="server" GroupName="EmailAddressEditable" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbEmailAddressEditableFalse" runat="server" GroupName="EmailAddressEditable" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                Phone Number Editable:&nbsp;
                <asp:RadioButton ID="rbPhoneNumberEditableTrue" runat="server" GroupName="PhoneNumberEditable" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbPhoneNumberEditableFalse" runat="server" GroupName="PhoneNumberEditable" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                CV2 Mandatory:&nbsp;
                <asp:RadioButton ID="rbCV2MandatoryTrue" runat="server" GroupName="CV2Mandatory" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbCV2MandatoryFalse" runat="server" GroupName="CV2Mandatory" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                Address 1 Mandatory:&nbsp;
                <asp:RadioButton ID="rbAddress1MandatoryTrue" runat="server" GroupName="Address1Mandatory" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbAddress1MandatoryFalse" runat="server" GroupName="Address1Mandatory" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                City Mandatory:&nbsp;
                <asp:RadioButton ID="rbCityMandatoryTrue" runat="server" GroupName="CityMandatory" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbCityMandatoryFalse" runat="server" GroupName="CityMandatory" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                Post Code Mandatory:&nbsp;
                <asp:RadioButton ID="rbPostCodeMandatoryTrue" runat="server" GroupName="PostCodeMandatory" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbPostCodeMandatoryFalse" runat="server" GroupName="PostCodeMandatory" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                State Mandatory:&nbsp;
                <asp:RadioButton ID="rbStateMandatoryTrue" runat="server" GroupName="StateMandatory" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbStateMandatoryFalse" runat="server" GroupName="StateMandatory" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                Country Mandatory:&nbsp;
                <asp:RadioButton ID="rbCountryMandatoryTrue" runat="server" GroupName="CountryMandatory" Text="True" Checked="true" />&nbsp;
                <asp:RadioButton ID="rbCountryMandatoryFalse" runat="server" GroupName="CountryMandatory" Text="False" Checked="false" />
            </div>
            <div class="form-group">
                <asp:Button ID="BtnSubmit" class="btn btn-primary" runat="server" Text="Submit For Processing" OnClick="BtnSubmit_Click" />
            </div>
        </div>
    </form>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
