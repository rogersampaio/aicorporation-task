﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PaymentForm.Model;
using System;

namespace PaymentForm.Controller.Tests
{
    [TestClass()]
    public class HashControllerTests
    {
        [TestMethod()]
        public void GetHashDigestGenericAmount1()
        {
            string hashInput = "PreSharedKey=5XNm04UE0/EaXDSeKa4Fw29hTzieAl32uO4=&MerchantID=Progra-7702818&Password=G6CH6Z90I2&Amount=1&CurrencyCode=978&EchoAVSCheckResult=&EchoCV2CheckResult=&EchoThreeDSecureAuthenticationCheckResult=&EchoFraudProtectionCheckResult=&EchoCardType=&EchoCardNumberFirstSix=&EchoCardNumberLastFour=&EchoCardExpiryDate=&EchoDonationAmount=&AVSOverridePolicy=&CV2OverridePolicy=&ThreeDSecureOverridePolicy=&OrderID=Order-1234&TransactionType=SALE&TransactionDateTime=2018-10-21 16:13:34 +00:00&DisplayCancelButton=&CallbackURL=&OrderDescription=&LineItemSalesTaxAmount=&LineItemSalesTaxDescription=&LineItemQuantity=&LineItemAmount=&LineItemDescription=&CustomerName=&DisplayBillingAddress=&Address1=&Address2=&Address3=&Address4=&City=&State=&PostCode=&CountryCode=&EmailAddress=&PhoneNumber=&DateOfBirth=&DisplayShippingDetails=&ShippingName=&ShippingAddress1=&ShippingAddress2=&ShippingAddress3=&ShippingAddress4=&ShippingCity=&ShippingState=&ShippingPostCode=&ShippingCountryCode=&ShippingEmailAddress=&ShippingPhoneNumber=&CustomerNameEditable=&EmailAddressEditable=&PhoneNumberEditable=&DateOfBirthEditable=&CV2Mandatory=false&Address1Mandatory=false&CityMandatory=false&PostCodeMandatory=false&StateMandatory=false&CountryMandatory=false&ShippingAddress1Mandatory=&ShippingCityMandatory=&ShippingPostCodeMandatory=&ShippingStateMandatory=&ShippingCountryMandatory=&ResultDeliveryMethod=POST&ServerResultURL=&PaymentFormDisplaysResult=&ServerResultURLCookieVariables=&ServerResultURLFormVariables=&ServerResultURLQueryStringVariables=&PrimaryAccountName=&PrimaryAccountNumber=&PrimaryAccountDateOfBirth=&PrimaryAccountPostCode=&Skin=&PaymentFormContentMode=&BreakoutOfIFrameOnCallback=";
            var result = HashController.GetHashDigest(hashInput);
            Assert.AreEqual("48e3f4f9663a775f81166beaa518addc6e92b641", result);
        }

        [TestMethod()]
        public void GetHashDigestGenericAmount10()
        {
            string hashInput = "PreSharedKey=5XNm04UE0/EaXDSeKa4Fw29hTzieAl32uO4=&MerchantID=Progra-7702818&Password=G6CH6Z90I2&Amount=10&CurrencyCode=978&EchoAVSCheckResult=&EchoCV2CheckResult=&EchoThreeDSecureAuthenticationCheckResult=&EchoFraudProtectionCheckResult=&EchoCardType=&EchoCardNumberFirstSix=&EchoCardNumberLastFour=&EchoCardExpiryDate=&EchoDonationAmount=&AVSOverridePolicy=&CV2OverridePolicy=&ThreeDSecureOverridePolicy=&OrderID=Order-1234&TransactionType=SALE&TransactionDateTime=2018-10-20 18:35:00&DisplayCancelButton=&CallbackURL=http://www.somedomain.com/callback.php&OrderDescription=&LineItemSalesTaxAmount=&LineItemSalesTaxDescription=&LineItemQuantity=&LineItemAmount=&LineItemDescription=&CustomerName=&DisplayBillingAddress=&Address1=&Address2=&Address3=&Address4=&City=&State=&PostCode=&CountryCode=&EmailAddress=&PhoneNumber=&DateOfBirth=&DisplayShippingDetails=&ShippingName=&ShippingAddress1=&ShippingAddress2=&ShippingAddress3=&ShippingAddress4=&ShippingCity=&ShippingState=&ShippingPostCode=&ShippingCountryCode=&ShippingEmailAddress=&ShippingPhoneNumber=&CustomerNameEditable=&EmailAddressEditable=&PhoneNumberEditable=&DateOfBirthEditable=&CV2Mandatory=false&Address1Mandatory=false&CityMandatory=false&PostCodeMandatory=false&StateMandatory=false&CountryMandatory=false&ShippingAddress1Mandatory=&ShippingCityMandatory=&ShippingPostCodeMandatory=&ShippingStateMandatory=&ShippingCountryMandatory=&ResultDeliveryMethod=POST&ServerResultURL=&PaymentFormDisplaysResult=&ServerResultURLCookieVariables=&ServerResultURLFormVariables=&ServerResultURLQueryStringVariables=&PrimaryAccountName=&PrimaryAccountNumber=&PrimaryAccountDateOfBirth=&PrimaryAccountPostCode=&Skin=&PaymentFormContentMode=&BreakoutOfIFrameOnCallback=";
            var result = HashController.GetHashDigest(hashInput);
            Assert.AreEqual("019d359bb5c5339d6d1deaf20eee08f2dfdf658d", result);
        }

        [TestMethod()]
        public void GetHashDigestWithPaymentModelAmount1()
        {
            DateTime transactionDateTime = new DateTime(2018, 10, 21, 16, 13, 34);
            Payment payment = new Payment("1", "978", transactionDateTime, "http://www.somedomain.com/callback.php",
                false, false, false, false, false, false, false, false, false, "A test order", "A Customer", "", "", "", "", "", "", "", "", "", "", false, false, false,
                false, false, false, false, false);
            var result = HashController.GetHashDigest(payment);
            Assert.AreEqual("8acfb29bfaecab4c86915092186b828c898fc295", result);
        }

        [TestMethod()]
        public void GetHashDigestWithPaymentModelAmount12()
        {
            DateTime transactionDateTime = new DateTime(2018, 10, 21, 16, 13, 34);
            Payment payment = new Payment("2", "978", transactionDateTime, "",
                true, true, true, true, true, true, true, true, true, "A test order", "A Customer", "", "", "", "", "", "", "", "", "", "", true, true, true,
                true, true, true, true, true);
            var result = HashController.GetHashDigest(payment);
            Assert.AreEqual("36a6a1b5d94c6bfa00574925b3696ad9e7916f33", result);
        }
    }
}